#!/bin/bash
echo "Launching 24XX VTT - Frontend container..."
echo "Setting filesystem permissions"

(chmod -R 777 /app)

echo "Starting 24XX VTT - Frontend..."
cd /app ; npm run start $@
