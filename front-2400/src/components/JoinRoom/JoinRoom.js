import React from 'react'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './JoinRoom.css'

export default function JoinRoom({ setJoin }) {
    const navigate = useNavigate()
    const [id, setId] = useState(null)

    const handleSubmit = function (e){
        e.preventDefault()
        if (id){
            navigate(`/rooms/${id}`)
        }
    }
    return (
        <>
            <form onSubmit={handleSubmit} className='container join-form'>
                <label className='row'>
                    <span>Room ID: </span>
                    <input required type="text" onChange={(e) => setId(e.target.value)}></input>
                </label>
                <div className='row'>
                    <button type="submit" className='btn btn-primary'>Join</button>
                    <button type="button" className='btn btn-primary' onClick={() => setJoin(false)}>Cancel</button>
                </div>
            </form>
        </>
    )
}
