import React from 'react'
import './CookiesWarn.css'

export default function CookiesWarn({setCookieAccepted}) {
    return (
        <div className='row cookies-wrapper'>
            <p>This website uses cookies to optimize perfomance and store user data</p>
            <button onClick={() => setCookieAccepted(true)}>Accept</button>
        </div>
    )
}
