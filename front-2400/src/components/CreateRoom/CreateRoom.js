import React from 'react'
import { useState, useEffect } from 'react'
import { createRoom } from '../../libs/queries'
import usePostFetch from '../../hooks/usePostFetch'
import { useNavigate } from 'react-router-dom'
import './CreateRoom.css'

export default function CreateRoom({ id, setCreate }) {
    const navigate = useNavigate()
    const [title, setTitle] = useState(null)
    const [description, setDescription] = useState(null)
    const [system, setSystem] = useState(null)
    const [password, setPassword] = useState(null)
    const [body, setBody] = useState(null)
    const [ data, loading, error ] = usePostFetch(`${process.env.REACT_APP_BACK_API}/graphql`, body)

    if (data){
        const id = data?.data?.createRoom?.data?.id || null
        id && navigate(`/rooms/${id}`)
    }

    const handleSubmit = function (e){
        e.preventDefault()
        if (title && id){
            setBody(createRoom({
                title: title,
                description: description,
                gm_id: id,
                system: system,
                password: password
            }))
        }
    }
    return (
        <>
            {error && <h1>{error}</h1>}
            {loading && <h1>Creating Room...</h1>}
            <form onSubmit={handleSubmit} className='container create-form'>
                <label className='row'>
                    <span>Room Title: </span>
                    <input required type="text" onChange={(e) => setTitle(e.target.value)}></input>
                </label>
                <label className='row'>
                    <span>24XX Game System: </span>
                    <input type="text" onChange={(e) => setSystem(e.target.value)}></input>
                </label>
                <label className='row'>
                    <span>Room Description: </span>
                    <textarea type="text" onChange={(e) => setDescription(e.target.value)}></textarea>
                </label>
                <label className='row'>
                    <span>Room Password: </span>
                    <input type="password" onChange={(e) => setPassword(e.target.value)}></input>
                </label>
                <div className='row'>
                    <button type="submit" className='btn btn-primary'>Create</button>
                    <button type="button" className='btn btn-primary' onClick={() => setCreate(false)}>Cancel</button>
                </div>
            </form>
        </>
    )
}
