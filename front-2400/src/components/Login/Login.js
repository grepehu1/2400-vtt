import React from 'react'
import { useState } from 'react'
import useLogin from '../../hooks/useLogin'
import './Login.css'

export default function Login() {
    const [user, setUser] = useState(null)
    const [submit, setSubmit] = useState(null)
    useLogin(user,submit)

    const handleLogin = function (e){
        e.preventDefault()
        if (user){
            setSubmit(true)
        }
    }

    return (
        <form onSubmit={handleLogin} className='container login-form'>
            <label className='row'>
                <span>Choose Your Nickname: </span>
                <input type="text" onChange={(e) => setUser(e.target.value)}></input>
            </label>
            <div className='row'>
                <button className='btn btn-primary'>Login</button>
            </div>
        </form>
    )
}
