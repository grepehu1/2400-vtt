import React, { useEffect } from 'react'
import useMainContext from './useMainContext'

export default function useLogin(user,submit) {
    const { changeUser, changeId } = useMainContext()

    useEffect(() => {
        if (submit) {
            const userId = generateUserID(user)
            
            document.cookie = `vtt2400-user-id=${userId}; SameSite=None; Secure`
            document.cookie = `vtt2400-username=${user}; SameSite=None; Secure`
            
            changeUser(user)
            changeId(userId)
        }
    }, [submit])


    return true
}

function generateUserID(user){
    const now = Date.now()
    return `${encodeURIComponent(user)}-${now}`
}