import React, { useContext } from 'react'
import { UserContext } from '../contexts/MainContext'

export default function useMainContext() {
    const context = useContext(UserContext)

    if (context === undefined) {
        throw new Error("useMainContext() must be used inside a context provider")
    }
    
    return context
}
