import React, { useState, useEffect } from 'react'
import useMainContext from "./useMainContext"

export default function useCheckLogin() {
    const [userState, setUserState] = useState(null)
    const [idState, setIdState] = useState(null)

    const { user, id, changeUser, changeId } = useMainContext()

    useEffect(() => {
        if (user && id ){

            setUserState(user)
            setIdState(id)
    
            return {
                user: userState,
                id: idState
            }
        } else {
            const cookies = document.cookie
            const cookiesArray = (cookies && String(cookies).split(';')) || []
            for (const cookie of cookiesArray){
                const pair = String(cookie).trim().split('=')
                const key = pair && pair[0]
                const value = pair && pair[1]
                if (key && value){
                    if (key === 'vtt2400-username'){
                        changeUser(value)
                        setUserState(value)
                    }else if (key === 'vtt2400-user-id'){
                        changeId(value)
                        setIdState(value)
                    }
                }
            }
        }
        
    }, [user,id])

    return {
        user: userState,
        id: idState
    }
}
