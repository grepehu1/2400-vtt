import React, { useState, useEffect } from 'react'
import useMainContext from './useMainContext'
import { useNavigate } from 'react-router-dom'

export default function useLogout() {
    const [ logout, setLogout ] = useState(null)
    const navigate = useNavigate()
    const { changeUser, changeId } = useMainContext()

    useEffect(() => {
        if (logout) {
            document.cookie = 'vtt2400-user-id=; SameSite=None; Secure'
            document.cookie = 'vtt2400-username=; SameSite=None; Secure'
            
            changeUser(null)
            changeId(null)

            navigate('/')
        }
    }, [logout])

    return setLogout
}