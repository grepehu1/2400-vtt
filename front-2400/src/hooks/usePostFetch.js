import React, { useState, useEffect } from 'react'

export default function usePostFetch(url, body) {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)

    useEffect(() => {
        const controller = new AbortController()

        if (url && body) {
            fetch(url, { 
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body),
                signal: controller.signal
            })
                .then(res => {
                    if (res.ok){
                        return res.json()
                    } else {
                        throw new Error(res.statusText)
                    }
                })
                .then(data => {
                    setData(data)
                    setLoading(false)
                    setError(false)
                })
                .catch(e => {
                    setLoading(false)
                    setError(e.message)
                })
        } else {
            setData(false)
            setLoading(false)
            setError(false)
        }

        return () => {
            controller.abort()
        }
    }, [url,body])

    return [data, loading, error]
}
