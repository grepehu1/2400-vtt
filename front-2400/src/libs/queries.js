module.exports = {
  roomList,
  fullRoomQuerySingle,
  createRoom
}

function createRoom({title, description = '', gm_id, system = '', password = ''}){
  if (!title || !gm_id) return

  return {
    query: `
      mutation createRoom{
        createRoom(data: {
          title: "${title}",
          description: "${description}",
          gm_id: "${gm_id}",
          system: "${system}",
          password: "${password}",
        }){
          data{
            id
          }
        }
      }
    `,
    variables: {}
  }
}

function roomList(){
  return {
    query: `
      query {
        rooms{
          data{
            id
            attributes{
              title
              description
              system
            }
          }
        }
      }
    `,
    variables: {}
  }
}

function fullRoomQuerySingle(id){
    return {
        query: `
        query singleRoom($id: ID!){
            room(id: $id){
              data{
                attributes{
                  title
                  description
                  system
                  background_id
                  gm_id
                  password
                  events{
                    id
                    title
                    content
                  }
                  npcs{
                    id
                    name
                    description
                    icon_id
                    items{
                      id
                      name
                      description
                      broken
                    }
                    special_features{
                      id
                      title
                      content
                    }
                  }
                  shown_npcs{
                    id
                    name
                    description
                    icon_id
                    items{
                      id
                      name
                      description
                      broken
                    }
                    special_features{
                      id
                      title
                      content
                    }
                  }
                  characters{
                    id
                    name
                    description
                    player_id
                    icon_id
                    skills{
                      id
                      name
                      level
                    }
                    items{
                      id
                      name
                      description
                      broken
                    }
                    special_feat{
                      id
                      title
                      content
                    }
                    other
                  }
                }
              }
            }
          }
        `,
        variables: {
            id
        }
    }
}