import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { fullRoomQuerySingle } from '../../libs/queries'
import usePostFetch from '../../hooks/usePostFetch'
import Login from '../../components/Login/Login'
import useCheckLogin from '../../hooks/useCheckLogin'

export default function Room() {
    const { id } = useParams()
    const { user, id: userId } = useCheckLogin()
    const [ body, setBody ] = useState(null)
    useEffect(() => {
        setBody(fullRoomQuerySingle(id))
    }, [id])
    const [ data, loading, error ] = usePostFetch(`${process.env.REACT_APP_BACK_API}/graphql`,body)

    const room = data?.data?.room?.data?.attributes || {}

    return (
        <>
            {(!user || !userId) && <Login />}
            {user && userId && (
                <div className='container rooms-wrapper'>
                    <div className='container room-list'>
                        {error && <h1>{error}</h1>}
                        {loading && <h1>Fetching Room Data...</h1>}
                        {data && (
                            <p>{JSON.stringify(room)}</p>
                        )}
                    </div>
                </div>
            )}
        </>
    )
}
