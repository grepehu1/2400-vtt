import React, { useState } from 'react'
import Login from '../../components/Login/Login'
import useCheckLogin from '../../hooks/useCheckLogin'
import useLogout from '../../hooks/useLogout'
import usePostFetch from '../../hooks/usePostFetch'
import { Link } from 'react-router-dom'
import './Rooms.css'
import { roomList } from '../../libs/queries'
import CreateRoom from '../../components/CreateRoom/CreateRoom'
import JoinRoom from '../../components/JoinRoom/JoinRoom'

const body = roomList()

export default function Rooms() {
    const { user, id } = useCheckLogin()
    const [ create, setCreate ] = useState(false)
    const [ join, setJoin ] = useState(false)
    const [rooms,loading,error] = usePostFetch(`${process.env.REACT_APP_BACK_API}/graphql`,body)

    const roomsArray = rooms?.data?.rooms?.data || [] 
    
    const logout = useLogout()
    
    return (
        <>
            {(!user || !id) && <Login />}
            {user && id && (
                <div className='container rooms-wrapper'>
                    <div className='row'>
                        <h1 className='rooms-title col-md-8'>Rooms</h1>
                        <div className='row col-md-4 user-greet'>
                            <h3 className='col-8'>Hello, { user }!</h3>
                            <button className='btn btn-danger col-4' onClick={() => logout(true)}>Logout</button>
                        </div>
                    </div>
                    <div className='container room-list'>
                        {error && <h1>{error}</h1>}
                        {loading && <h1>Fetching Rooms...</h1>}
                        {roomsArray.map(room => (
                            <Link to={`/rooms/${room.id}`} key={room.id} className='row'>
                                <div className='col-md-4'>
                                    {room.attributes.title && (
                                        <h3>{room.attributes.title}</h3>
                                    )}
                                    {room.attributes.system && (
                                        <i>system: {room.attributes.system}</i>
                                    )}
                                </div>
                                {room.attributes.description && (
                                    <p className='col-md-8'>{room.attributes.description}</p>
                                )}
                            </Link>
                        ))}
                    </div>
                    <div className='row rooms-panel'>
                        <div className='col-md-6'>
                            <button onClick={() => setCreate(true)}>Create</button>
                        </div>
                        <div className='col-md-6'>
                            <button onClick={() => setJoin(true)}>Join</button>
                        </div>
                    </div>
                    {create && <CreateRoom id={id} setCreate={setCreate}/>}
                    {join && <JoinRoom setJoin={setJoin}/>}
                </div>
            )}
        </>
    )
}
