import React from 'react'
import { Link } from 'react-router-dom'
import './Home.css'

export default function Home() {
    return (
        <div className='main-menu'>
            <div className='container title-wrapper'>
                <h1>24XX Online</h1>
            </div>
            <div className='container menu-list'>
                <div className='row'>
                    <Link to='/rooms'>Play</Link>
                </div>
                <div className='row'>
                    <Link to='/'>About</Link>
                </div>
                <div className='row'>
                    <Link to='/'>How to play</Link>
                </div>
                <div className='row'>
                    <Link to='/'>Resources</Link>
                </div>
                <div className='row'>
                    <Link to='/'>Credits</Link>
                </div>
            </div>
        </div>
    )
}
