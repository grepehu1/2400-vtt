import { useState } from 'react';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import './assets/css/global.css';
import Home from './pages/Home/Home'
import Rooms from './pages/Rooms/Rooms';
import Room from './pages/Room/Room';
import Admin from './pages/Admin/Admin';
import CookiesWarn from './components/CookiesWarn/CookiesWarn';

function App() {
  const [cookieAccepted, setCookieAccepted] = useState(false)

  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/admin" element={<Admin />} />
          <Route exact path="/rooms" element={<Rooms />} />
          <Route exact path="/rooms/:id" element={<Room />} />
        </Routes>
      </Router>
      {!cookieAccepted && <CookiesWarn setCookieAccepted={setCookieAccepted}/>}
    </>
  );
}

export default App;
