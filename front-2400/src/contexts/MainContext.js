import React, {createContext, useReducer} from 'react'

export const UserContext = createContext()

const userReducer = (state, action) => {
  switch (action.type) {
    case 'CHANGE_USER':
        return { ...state, user: action.payload }
    case 'CHANGE_ID':
        return { ...state, id: action.payload }
    case 'CHANGE_LANG':
        return { ...state, lang: action.payload }
    default:
        return state
  }
}

export function MainContext({children}) {
    const [state, dispatch] = useReducer(userReducer, {
        lang: 'en'
    })

    const changeUser = (user) => {
        dispatch({ type: 'CHANGE_USER', payload: user })
    }

    const changeId = (id) => {
        dispatch({ type: 'CHANGE_ID', payload: id })
    }

    const changeLang = (lang) => {
        dispatch({ type: 'CHANGE_LANG', payload: lang })
    }
    
    return (
        <UserContext.Provider value={{...state,changeUser,changeId,changeLang}}>
            {children}
        </UserContext.Provider>
    )
}
