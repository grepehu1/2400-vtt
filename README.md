# 24XX VTT Webapp

## Introduction

This project is a simple webapp VTT to play the micro-TTRPG 24XX.

## Initial Setup

At first run the command `make setup` so it will install all the dependencies locally in your computer to make sure you map the files correctly inside the containers;

It will take some time, but once the containers are running you may run in another terminal `make grant` to give you access to all files in the directory, and finally `make importDump` to import the SQL Dump for development and testing purposes that come with this repository, inside the folder "/dump".

For any consecutive runs, you may simply start the servers with `make up`.

## Dependencies

- Strapi 4.0.3
- PostgreSQL 14.1
- React.JS 17.0.2
- Bootstrap 5.1.3
- Socket.io 4.4.1

## Spinning It Up

- `make setup`: Initial setup for development environment
- `make up`: Spins up the necessary containers.
- `make down`: Turn down containers and remove everything created with them. 
- `make upBuild`: Spins up the necessary containers forcing their docker images to rebuild.
- `make grant`: Grants admin access to all files in case you run into privilege problems.
- `make accessBack`: Access backend container.
- `make accessFront`: Access frontend container.
- `make accessWebsocket`: Access websocket container.
- `make install`: Install NPM packages inside the containers.
- `make build`: Run NPM build inside the containers.
- `make importDump`: Import the dump/dump.sql file into the database.
- `make exportDump`: Export the dump/dump.sql file from the database.

## Important Links (DEV)

- `http://localhost:3000`: React frontend.
- `http://localhost:3000/admin`: Strapi admin panel.
- `http://localhost:1337/admin`: Also Strapi admin panel. 
- `http://localhost:5000`: Websocket server.

## DEV Test Users

- `Super User`: User: "super@user.com", Password: "SuperUser1".
- `Room GM`: User: "test1@test.com", Password: "123456".
- `Player 2`: User: "test2@test.com", Password: "123456".
- `Player 3`: User: "test3@test.com", Password: "123456".
- `Player 4`: User: "test4@test.com", Password: "123456".