module.exports = {
  
    '0 0 * * * *': async ({ strapi }) => {
        const { count } = await strapi.service('api::room.room').unusedRooms()
        console.log(`${count} rooms were delete due to inactivity`)
    },
};