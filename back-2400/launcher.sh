#!/bin/bash
echo "Launching VTT 24XX - Backend container..."
echo "Setting filesystem permissions"

(chmod -R 777 /app)

echo "Starting VTT 24XX - Backend..."
cd /app ; npm run develop $@
