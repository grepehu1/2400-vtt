'use strict';

/**
 * room service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::room.room', ({ strapi }) =>  ({
    async unusedRooms() {
        const now = new Date()
        const before = new Date(now.getTime() - (4 * 60 * 60 * 1000))

        let count = await strapi.db.query('api::room.room').deleteMany({
                where: { 
                    updatedAt: {
                        $lt: before
                    }
                }
        })
  
        return count
    },
}));
