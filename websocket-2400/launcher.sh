#!/bin/bash
echo "Launching 24XX VTT - Websocket container..."
echo "Setting filesystem permissions"

(chmod -R 777 /app)

echo "Starting 24XX VTT - Websocket..."
cd /app ; npm run start $@
